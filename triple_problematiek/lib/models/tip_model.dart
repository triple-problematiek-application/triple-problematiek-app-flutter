import 'dart:convert';

class Tip {

  final String title;
  final String cover;
  final String description;
  final List<String> positiveBulletPoints;
  final List<String> negativeBulletPoints;
  final String owner;
  final DateTime date;

  Tip(this.title, this.cover, this.description, this.positiveBulletPoints,
      this.negativeBulletPoints, this.owner, this.date);

  static Tip fromJson(Map<String, dynamic> tip) {
    return Tip(
      tip['title'],
      tip['cover'],
      tip['description'],
      tip['positiveBulletPoints'].cast<String>(),
      tip['negativeBulletPoints'].cast<String>(),
      "${tip['owner']['firstName']} ${tip['owner']['lastName']}",
      DateTime.parse(tip['date']),
    );
  }

  @override
  String toString() {
    return jsonEncode(
      {
        "title": this.title,
        "cover": this.cover,
        "description": this.description,
        "owner": this.owner,
        "positiveBulletPoints": this.positiveBulletPoints,
        "negativeBulletPoints": this.negativeBulletPoints,
      },
    );
  }
}
