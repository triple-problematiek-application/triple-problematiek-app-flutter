import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class User {

  final String username;
  final String email;
  final String password;
  final bool administrator;
  final String profilePicture;

  User(this.username, this.email, this.password, this.administrator, this.profilePicture);

  static void saveUser(User user) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("user", user.toString());
  }

  static Future<User> fromJson() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userString = preferences.getString("user");

    if (userString == null || userString.isEmpty) return null;

    String jsonString = jsonDecode(userString);

    //TODO: Fix encoding
    /*return User (
      jsonString["username"] as String;
      jsonString["email"] as String;
      jsonString["password"] as String;
      jsonString["administrator"] as bool;
      jsonString["profilePicture"] as String;
    );*/
  }
  
  @override
  String toString() {
    return jsonEncode({
      "username": this.username,
      "email": this.email,
      "password": this.password,
      "administrator": this.administrator,
      "profilePicture": this.profilePicture
    });
  }

}