import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Disorder {
  
  final String title;
  final String definition;
  final String description;
  final List<String> symptoms;
  final String dealingWith;

  Disorder(this.title, this.definition, this.description, this.symptoms, this.dealingWith);


  static Disorder fromJson(Map<String, dynamic> disorder) {
    return Disorder(
      disorder['title'],
      disorder['definition'],
      disorder['description'],
      disorder['symptoms'].cast<String>(),
      disorder['dealingWith'],
    );
  }
  
  @override
  String toString() {
    return jsonEncode({
      "title": this.title,
      "definition": this.definition,
      "description": this.description,
      "symptoms": this.symptoms,
      "dealingWith": this.dealingWith,
    });
  }

}