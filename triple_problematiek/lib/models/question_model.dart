import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'answer_model.dart';

class Question {

  final String title;
  final String category;
  final String body;
  final String objectId;
  final String author;
  final DateTime date;
  List<Answer> answers;
  final int likes;
  final int comments;

  Question(this.title, this.category, this.body, this.objectId, this.author, this.date, this.answers, this.likes, this.comments);

  @override
  String toString() {
    return jsonEncode({
      "title": this.title,
      "category": this.category,
      "body": this.body,
      "objectId": this.objectId
    });
  }

}