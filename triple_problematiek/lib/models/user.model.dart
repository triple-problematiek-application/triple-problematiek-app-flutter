import 'dart:convert';

class User {
  final String firstName;
  final String lastName;
  final String function;
  final String emailAddress;
  final String password;
  final String token;
  final String phoneNumber;
  final String profilePicture;
  final bool isAdministrator;

  User(
      {this.firstName,
      this.lastName,
      this.function,
      this.emailAddress,
      this.password,
      this.token,
      this.phoneNumber,
        this.profilePicture,
      this.isAdministrator});

  String toLoginJSON() {
    return jsonEncode(
        {"emailAddress": this.emailAddress, "password": this.password});
  }

  String toJson() {
    print(this.firstName);
    return jsonEncode({
      "firstName": this?.firstName,
      "lastName": this?.lastName,
      "function": this?.function,
      "emailAddress": this?.emailAddress,
      "password": this?.password,
      "token": this?.token,
      "profilePicture": this?.profilePicture,
      "phoneNumber": this?.phoneNumber,
      "isAdministrator": this?.isAdministrator,
    });
  }

  static User fromJson(String userString) {
    Map<String, dynamic> user = jsonDecode(userString);
    print(user);
    return User(
        firstName: user['firstName'],
        lastName: user['lastName'],
        function: user['function'],
        emailAddress: user['emailAddress'],
        token: user['token'],
        profilePicture: user['profilePicture'],
        phoneNumber: user['phoneNumber'],
        isAdministrator: user['isAdministrator']);
  }
}
