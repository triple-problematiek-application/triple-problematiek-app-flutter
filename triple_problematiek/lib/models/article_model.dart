import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Article {
  final String cover;
  final String id;
  final String title;
  final String category;
  final String body;
  final String owner;
  final DateTime date;

  Article(this.cover, this.id, this.title, this.category, this.body, this.owner, this.date);

  static void saveArticle(Article article) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString("article", article.toString());
  }

  static Article fromJson(Map<String, dynamic> jsonString) {
    return Article(
      jsonString['cover'],
      jsonString['_id'],
      jsonString['title'],
      jsonString['category'],
      jsonString['body'],
      "${jsonString['owner']['firstName']} ${jsonString['owner']['lastName']}",
      DateTime.parse(jsonString['date']),
    );
  }

  @override
  String toString() {
    return jsonEncode({
      "cover": this.cover,
      "id": this.id,
      "title": this.title,
      "category": this.category,
      "body": this.body,
      "owner": this.owner,
      "date": this.date.toString(),
    });
  }
}
