import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Answer {

  final String author;
  final String body;
  final DateTime date;

  Answer(this.author, this.body, this.date);

}