import 'package:flutter/material.dart';
import 'package:tripleproblematiek/screens/Home/home_screen.dart';
import 'package:tripleproblematiek/screens/loading.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        scaffoldBackgroundColor: Colors.white,
        appBarTheme: AppBarTheme(
            color: Color(0xFFFED42E),
            textTheme: TextTheme(
                headline6: TextStyle(
              color: Colors.white,
            )),
            iconTheme: IconThemeData(
              color: Colors.white,
            )),
      ),
      home: MyHomePage(title: 'Home'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    load();
  }

  void load() async {
    await SharedPreferencesService.init();
    Future.delayed(
      Duration(seconds: 1),
      () {
        Navigator.of(context).pop();
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[
          Expanded(child: LoadingScreen()),
        ],
      ),
    );
  }
}
