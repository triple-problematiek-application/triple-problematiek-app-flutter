import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/models/user.model.dart';
import 'package:tripleproblematiek/services/network_services/article_service.dart';

class SharedPreferencesService {

  static List<Article> _articles = List();
  static SharedPreferences _sharedPreferences;
  static User _user;

  static Future<void> init() async {
    print('Loading data');
    _sharedPreferences = await SharedPreferences.getInstance();
    if (isLoggedIn()) _user = User.fromJson(_sharedPreferences.getString("user"));
    Map<String, dynamic> articles = await ArticleService.getArticles(null);
    for (var articleObject in articles['articles']) {
      _articles.add(
        Article.fromJson(articleObject),
      );
    }
    print("Loading data ok!");
    return;
  }


  static List<Article> get articles => _articles;

  static User get user => _user;

  static bool isLoggedIn() {
    return _sharedPreferences.getString("user") != null;
  }

  static void login(User user) {
    _sharedPreferences.setString("user", user.toJson());
    _user = user;
  }

  static void logout() {
    _sharedPreferences.setString("user", null);
    _user = null;
  }
}
