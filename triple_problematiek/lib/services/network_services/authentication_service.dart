import 'dart:convert';

import 'package:tripleproblematiek/models/user.model.dart';
import 'package:http/http.dart' as http;
import 'package:tripleproblematiek/services/base_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class AuthenticationService {
  static Future<Map<String, dynamic>> registerUser(User userProperties) async {
    if (userProperties.isAdministrator)
      throw Exception("User must be a normal user");
    print(userProperties.toJson());
    final response = await http.post(
      "${BaseService.BASE_URL}user",
      headers: {"Content-type": "application/json"},
      body: userProperties.toJson(),
    );

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> loginUser(User userProperties) async {
    if (userProperties.isAdministrator)
      throw Exception("User must be a normal user");
    final response = await http.put(
      "${BaseService.BASE_URL}login",
      headers: {"Content-type": "application/json"},
      body: userProperties.toJson(),
    );

    Map<String, dynamic> json = jsonDecode(response.body);

    return json;
  }

  static Future<Map<String, dynamic>> updateUser(User userProperties) async {
    print(userProperties.toJson());

    Map<String, dynamic> body;


    final response = await http.put(
      "${BaseService.BASE_URL}user",
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer ${SharedPreferencesService.user.token}"
      },
      body: userProperties.toJson(),
    );

    Map<String, dynamic> json = jsonDecode(response.body);

    return json;
  }
}
