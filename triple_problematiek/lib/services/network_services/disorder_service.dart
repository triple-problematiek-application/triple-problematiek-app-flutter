import 'dart:convert';

import 'package:tripleproblematiek/models/disorder_model.dart';

import 'package:http/http.dart' as http;
import 'package:tripleproblematiek/services/base_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class DisorderService {

  static Future<Map<String, dynamic>> postDisorder(Disorder disorder) async {
    final response = await http.post("${BaseService.BASE_URL}disorder",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer ${SharedPreferencesService.user.token}"
        },
        body: disorder.toString());

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> getDisorders(String query) async {
    String url = "${BaseService.BASE_URL}disorder";
    if (query != null) url += "/?title=$query";
    final response = await http.get(url,
        headers: {"Content-type": "application/json"});

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

}