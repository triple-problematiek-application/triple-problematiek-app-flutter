import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tripleproblematiek/models/tip_model.dart';
import 'package:tripleproblematiek/services/base_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class TipService {
  static Future<Map<String, dynamic>> postTip(Tip tip) async {
    final response = await http.post("${BaseService.BASE_URL}tip",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer ${SharedPreferencesService.user.token}"
        },
        body: tip.toString());

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> getTips(String query) async {
    String url = "${BaseService.BASE_URL}tip";
    if (query != null) url += "/?title=$query";
    print(url);
    final response = await http.get(
      url,
      headers: {"Content-type": "application/json"},
    );

    Map<String, dynamic> json = jsonDecode(response.body);
    print(json);
    return json;
  }

  static Future<Map<String, dynamic>> getMyTips(String query) async {
    String url = "${BaseService.BASE_URL}tip/myTips";
    if (query != null) url += "/?title=$query";
    final response = await http.get(
      url,
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer ${SharedPreferencesService.user.token}"
      },
    );

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }
}
