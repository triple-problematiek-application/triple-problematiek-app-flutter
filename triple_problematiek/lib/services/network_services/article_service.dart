import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/services/base_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class ArticleService {
  static Future<Map<String, dynamic>> postArticle(Article article) async {
    final response = await http.post("${BaseService.BASE_URL}article",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer ${SharedPreferencesService.user.token}"
        },
        body: article.toString());

    print(response.body);
    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> deleteArticle(String id) async {
    final response =
        await http.delete("${BaseService.BASE_URL}article/$id", headers: {
      "Content-type": "application/json",
      "Authorization": "Bearer ${SharedPreferencesService.user.token}"
    });

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> updateArticle(Article article) async {
    final response =
        await http.put("${BaseService.BASE_URL}article/${article.id}",
            headers: {
              "Content-type": "application/json",
              "Authorization": "Bearer ${SharedPreferencesService.user.token}"
            },
            body: article.toString());

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> verifyArticle(Article article) async {
    final response = await http.put("${BaseService.BASE_URL}article/${article.id}/verify",
        headers: {
          "Content-type": "application/json",
          "Authorization": "Bearer ${SharedPreferencesService.user.token}"
        },
        body: article.toString());

    Map<String, dynamic> json = jsonDecode(response.body);
    print(json);
    return json;
  }

  static Future<Map<String, dynamic>> getArticles(String query) async {
    String url = "${BaseService.BASE_URL}article";
    if (query != null) url += "/?title=$query";
    final response = await http.get(url,
        headers: {"Content-type": "application/json"});

    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> getOwnArticles(String query) async {
    String url = "${BaseService.BASE_URL}article/myArticles";
    if (query != null) url += "/?title=$query";
    final response = await http.get(
      url,
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer ${SharedPreferencesService.user.token}"
      },
    );
    print(response.body);
    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }

  static Future<Map<String, dynamic>> getUnverifiedArticles(String query) async {
    String url = "${BaseService.BASE_URL}article/unverified";
    if (query != null) url += "/?title=$query";
    final response = await http.get(
      url,
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer ${SharedPreferencesService.user.token}"
      },
    );
    Map<String, dynamic> json = jsonDecode(response.body);
    return json;
  }
}
