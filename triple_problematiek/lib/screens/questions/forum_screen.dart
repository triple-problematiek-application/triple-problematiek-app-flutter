import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/answer_model.dart';
import 'package:tripleproblematiek/models/question_model.dart';
import 'package:tripleproblematiek/screens/questions/widgets/question_card.dart';
import 'package:tripleproblematiek/widgets/searchbar.dart';

import 'question_add.dart';

class ForumScreen extends StatefulWidget {
  @override
  _ForumScreenState createState() => _ForumScreenState();
}

class _ForumScreenState extends State<ForumScreen> {
  @override
  Widget build(BuildContext context) {
    
    List<Answer> answers = [
    ];

    List<Question> questions = [
    Question("Titel van de vraag", "Categorie", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "12", "Jeanne Klaver", DateTime.now(), answers, 15, answers.length),
    Question("Titel van de tweede vraag", "Categorie", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "12", "Jeanne Klaver", DateTime.now(), answers, 69, answers.length),
    ];

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFFEC5C27),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => AddQuestion(),
            ),
          );
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(left: 15, right: 15, top: 35, bottom: 27),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Forum',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 10),
              ClipRRect(
                borderRadius: BorderRadius.circular(3.0),
                child: Container(
                  padding: EdgeInsets.all(6),
                  width: 370,
                  height: 58,
                  color: Color(0xFFEB5F52),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.info,
                        color: Colors.white,
                      ),
                      SizedBox(width: 6),
                      Flexible(
                        child: Text(
                          "Hier komt een disclaimer te staan die aangeeft dat het forum niet bedoeld is voor professionele hulp",
                          style: TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 25),
             SearchBarWidget("Zoek op een vraag"),
              SizedBox(height: 25),
              Container(
                width: double.infinity, //TODO: Check if needed
                child: ListView.builder(
                  itemCount: questions.length,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return QuestionCard(
                      index: index,
                      question: questions[index],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );

  }
}