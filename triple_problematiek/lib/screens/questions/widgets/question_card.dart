import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tripleproblematiek/models/question_model.dart';

import '../question_screen.dart';

class QuestionCard extends StatelessWidget {
  final Question question;
  final int index;

  QuestionCard({this.question, this.index});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => QuestionScreen(
              question: question,
              index: index,
            ),
          ),
        );
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(3.0),
        child: Container(
          margin: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 5),
          height: 155,
          decoration:
              BoxDecoration(border: Border.all(color: Color(0xFFCCCCCC))),
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  question.title,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    height: 1.65,
                    color: Color(0xFF3E3E3E),
                  ),
                ),
                Text(
                  DateFormat("dd-MM-yyyy - kk:mm").format(question.date),
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14,
                    color: Color(0xFF3E3E3E),
                    fontStyle: FontStyle.italic,
                  ),
                ),
                SizedBox(height: 5),
                Flexible(
                  child: Text(
                    question.body,
                    maxLines: 3,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 14,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(
                        Icons.thumb_up,
                        size: 16.0,
                        color: Color(0xFF3E3E3E),
                      ),
                      onTap: () {
                        print('test of het werkt');
                      },
                    ),
                    SizedBox(width: 5,),
                    Text(
                      question.likes.toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 12,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                    SizedBox(width: 10),
                    Icon(Icons.comment, size: 16.0, color: Color(0xFF3E3E3E),),
                    SizedBox(width: 10),
                    Text(
                      question.comments.toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 12,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                    Spacer(),
                    GestureDetector(
                      child: Icon(Icons.delete,
                          size: 16.0, color: Color(0xFF3E3E3E)),
                      onTap: () {
                        print('test of het wordt verwijderd');
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
    throw UnimplementedError();
  }
}
