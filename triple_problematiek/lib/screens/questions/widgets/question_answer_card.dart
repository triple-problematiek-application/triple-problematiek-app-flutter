import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tripleproblematiek/models/answer_model.dart';

class AnswerCard extends StatelessWidget{

  final Answer answer;
  final int index;

  AnswerCard({this.answer, this.index});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(3.0),
      child: Expanded(
        child: Container(
          margin: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 5),
          decoration: BoxDecoration(
              border: Border.all(color: Color(0xFFCCCCCC))
          ),
          child: Container(
            margin: EdgeInsets.only(left: 5, right: 5, top: 0, bottom: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  //TODO: get name of logged in person 
                  answer.author,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    height: 1.65,
                    color: Color(0xFF3E3E3E),
                  ),
                ),
                Text(
                  DateFormat("dd-MM-yyyy - kk:mm").format(answer.date),
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14,
                    color: Color(0xFF3E3E3E),
                    fontStyle: FontStyle.italic,
                  ),
                ),
                SizedBox(height: 15),
                Text(
                   answer.body,
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: Color(0xFF3E3E3E),
                  ),
                ),
                SizedBox(height: 15),
              ],
            ),
          ),
        ),
      ),
    );
    throw UnimplementedError();
  }

}