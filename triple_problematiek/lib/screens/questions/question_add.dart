import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/widgets/Buttons/button.dart';
import 'package:tripleproblematiek/widgets/Buttons/button_border.dart';
import 'package:tripleproblematiek/widgets/image_picker.dart';
import 'package:zefyr/zefyr.dart';

class AddQuestion extends StatefulWidget {
  @override
  _AddQuestionState createState() => _AddQuestionState();
}

class _AddQuestionState extends State<AddQuestion> {
  var categories = [
    'Technisch',
    'Atechnisch',
];
  var currentItemSelected;

  @override
  Widget build(BuildContext context) {
    // Return the actual Scaffold with the body we just made ('form')
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Nieuwe vraag',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 15),
                Text(
                  'Maak een nieuwe vraag aan',
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
                SizedBox(height: 25),
                Text(
                  'Titel',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 5,
                ),
                TextField(
                  maxLines: null,
                  decoration: InputDecoration(
                      border:
                      OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'De titel van de vraag'
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Text(
                  'Bericht',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(height: 5),
                TextField(
                  maxLines: 6,
                  decoration: InputDecoration(
                      border:
                      OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'Het bericht...'
                  ),
                ),
                SizedBox(height: 25),
                Text(
                  'Categorie',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(height: 5),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey)),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Text('Selecteer categorie'),
                      items: categories.map((String dropDownStringItem) {
                        return DropdownMenuItem<String>(
                          value: dropDownStringItem,
                          child: Text(dropDownStringItem),
                        );
                      }).toList(),
                      onChanged: (String newValueSelected) {
                        setState(() {
                          this.currentItemSelected = newValueSelected;
                        });
                      },
                      value: currentItemSelected,
                    ),
                  ),
                ),
                SizedBox(height: 35),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonWidget(title: "Opslaan", onClick: () => null,),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonBorderWidget("Annuleren"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
