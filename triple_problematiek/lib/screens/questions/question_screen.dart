import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tripleproblematiek/models/answer_model.dart';
import 'package:tripleproblematiek/models/question_model.dart';
import 'package:tripleproblematiek/screens/questions/widgets/question_answer_card.dart';
import 'package:tripleproblematiek/widgets/Buttons/button.dart';
import 'package:tripleproblematiek/widgets/category.dart';
import 'package:zefyr/zefyr.dart';

class QuestionScreen extends StatefulWidget {

  final Question question;
  final int index;

  QuestionScreen({this.question, this.index});

  @override
  _QuestionScreenState createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  final textController = TextEditingController();

  @override
  void setState(fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 36, bottom: 42),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.question.title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500,), ),
                SizedBox(height: 15),
                ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: Text(widget.question.body,
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                SizedBox(height: 15),
                Text(widget.question.author,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      height: 1.65,
                      color: Color(0xFF3E3E3E),
                    )),
                Text(DateFormat("dd-MM-yyyy - kk:mm").format(widget.question.date),
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 12,
                      height: 1.65,
                      color: Color(0xFF3E3E3E),
                    )),
                SizedBox(height: 25),
                CategoryWidget(widget.question.category),
                SizedBox(height: 35),
                Text("Antwoorden (" + widget.question.answers.length.toString() + ")", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,),),
                SizedBox(height: 15),
                Container(
                  width: double.infinity, //TODO: Check if needed
                  child: ListView.builder(
                    itemCount: widget.question.answers.length,
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                      child: AnswerCard(
                        index: index,
                        answer: widget.question.answers[index],
                      ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 25),
                Text("Plaats een reactie", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,),),
                SizedBox(height: 10),
                TextField(
                  maxLines: 6,
                  controller: textController,
                  decoration: InputDecoration(
                      border:
                      OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'Typ hier een reactie'
                  ),
                ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonWidget(
                            title: "Bevestigen",
                            onClick: () =>
                          {widget.question.answers.add(
                            Answer("Author", textController.text, DateTime.now()),
                            ),
                            setState(() {widget.question.answers = widget.question.answers;
                            textController.text = "";
                            })
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
