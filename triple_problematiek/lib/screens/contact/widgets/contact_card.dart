import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';

// ignore: must_be_immutable
class ContactCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: Color(0xFFCCCCCC),
            width: 0.0
        ),
      ),
      child: ExpandablePanel(
        theme: ExpandableThemeData(
            iconColor: Color(0xFF3e3e3e).withOpacity(0.8),
            iconPadding: EdgeInsets.only(top: 30, right: 18)
        ),
        header: Container(
          height: 87,
          width: double.infinity,
          padding: EdgeInsets.only(left: 18, right: 18, top: 21, bottom: 21),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(90),
                child: Image(
                  image: AssetImage('assets/images/neomi.jpg'),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Neomi van Duijvenbode",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      "Auteur",
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 14.0,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        expanded: Container(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Neomi van Duijvenbode is een van de auteurs van het boek 'Triple Problematiek'",
                style: TextStyle(
                  height: 1.65,
                  fontWeight: FontWeight.w400,
                  fontSize: 14.0,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 15),
              Text(
                "Contact",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 5),
              Text(
                "n.vanduijvenbode@tactus.nl",
                style: TextStyle(
                  height: 1.65,
                  fontWeight: FontWeight.w400,
                  fontSize: 14.0,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              Text(
                "+31612345678",
                style: TextStyle(
                  height: 1.65,
                  fontWeight: FontWeight.w400,
                  fontSize: 14.0,
                  color: Color(0xFF3E3E3E),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
