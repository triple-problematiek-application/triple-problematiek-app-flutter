import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/tip_model.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_image.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_title.dart';
import 'package:tripleproblematiek/screens/tip/tip_screen.dart';

class TipCard extends StatelessWidget {
  final Tip tip;
  final int index;

  TipCard({this.tip, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: InkWell(
        onTap: () => {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => TipScreen(
                index: index,
                tip: tip,
              ),
            ),
          ),
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 104,
              height: 70,
              child: ArticleImage(
                index: index,
                small: true,
                encode: tip.cover,
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ArticleTitle(
                      text: tip.title,
                      index: index,
                      inListView: true,
                    ),
                    SizedBox(height: 5),
                    Text(
                      tip.owner,
                      maxLines: 2,
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        height: 1.65,
                        color: Color(0xFF3E3E3E),
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}