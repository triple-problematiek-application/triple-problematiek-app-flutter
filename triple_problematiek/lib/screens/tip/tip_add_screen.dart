import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/tip_model.dart';
import 'package:tripleproblematiek/services/network_services/tip_service.dart';
import 'package:tripleproblematiek/widgets/Buttons/button.dart';
import 'package:tripleproblematiek/widgets/Buttons/button_border.dart';
import 'package:tripleproblematiek/widgets/bullet_point_widget/bullet_point_input.dart';
import 'package:tripleproblematiek/widgets/image_picker.dart';
import 'package:tripleproblematiek/widgets/text/form_field_title.dart';
import 'package:tripleproblematiek/widgets/text/form_title.dart';

class AddTip extends StatefulWidget {
  @override
  _AddTipState createState() => _AddTipState();
}

class _AddTipState extends State<AddTip> {
  final _formKey = GlobalKey<FormState>();

  String _image;

  final TextEditingController _title = TextEditingController();
  final TextEditingController _description = TextEditingController();

  final BulletPoint positive = BulletPoint("Do's", "Voeg een do toe");
  final BulletPoint negative = BulletPoint("Dont's", "Voeg een don't toe");

  @override
  void initState() {
    super.initState();
  }

  void submit() async {
    if (_formKey.currentState.validate()) {
      Tip tip = Tip(_title.value.text, _image, _description.value.text,
          positive.getPoints(), negative.getPoints(), "", null);
      Map<String, dynamic> response = await TipService.postTip(tip);
      Navigator.of(context).pop();
      print(response);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFFED42E),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FormTitle(
                  "Nieuwe tip",
                  "Maak een nieuwe tip aan",
                ),
                SizedBox(height: 35),
                ImagePickerWidget((image) => _image = image),
                SizedBox(height: 25),
                FormFieldTitle('Titel'),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                  maxLines: null,
                  controller: _title,
                  validator: (value) {
                    if (value.isEmpty) return 'Voer een titel in';
                    return null;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'De titel van de tip'),
                ),
                SizedBox(
                  height: 25,
                ),
                FormFieldTitle('Omschrijving'),
                SizedBox(height: 5),
                TextFormField(
                  maxLines: 6,
                  controller: _description,
                  validator: (value) {
                    if (value.isEmpty) return 'Voer een beschrijving in';
                    return null;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'Tip omschrijving'),
                ),
                SizedBox(height: 25),
                FormFieldTitle("Do's"),
                SizedBox(height: 5),
                positive,
                SizedBox(height: 25),
                FormFieldTitle("Dont's"),
                negative,
                SizedBox(height: 25),
                SizedBox(height: 35),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonWidget(
                          title: "Opslaan",
                          onClick: () => submit(),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonBorderWidget("Annuleren"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
