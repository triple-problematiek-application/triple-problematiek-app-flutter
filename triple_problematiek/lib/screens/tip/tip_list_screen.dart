import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/tip_model.dart';
import 'package:tripleproblematiek/screens/article/utils/request_type.dart';
import 'package:tripleproblematiek/screens/tip/tip_add_screen.dart';
import 'package:tripleproblematiek/services/network_services/tip_service.dart';
import 'package:tripleproblematiek/widgets/searchbar.dart';
import 'package:tripleproblematiek/screens/tip/widgets/tip_list_item.dart';

class TipListScreen extends StatefulWidget {
  @override
  _TipListScreenState createState() => _TipListScreenState();

  final RequestType type;

  TipListScreen(this.type);
}

class _TipListScreenState extends State<TipListScreen> {
  List<Tip> tips = List();

  @override
  void initState() {
    super.initState();
    loadTips(null);
  }

  void loadTips(String query) async {
    print(query);
    Map<String, dynamic> loadedTips = widget.type == RequestType.NORMAL
        ? await TipService.getTips(query)
        : await TipService.getMyTips(query);

    setState(() {
      tips = List();
      for (var tipObject in loadedTips['tips']) {
        tips.add(Tip.fromJson(tipObject));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFFEC5C27),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => AddTip(),
            ),
          );
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 27),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.type == RequestType.NORMAL ? 'Tips' : 'Mijn tips',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 25),
              SearchBarWidget("Zoek een tip", (value) => loadTips(value)),
              SizedBox(height: 25),
              Container(
                width: double.infinity, //TODO: Check if needed
                child: ListView.builder(
                  itemCount: tips.length,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return TipCard(
                      index: index,
                      tip: tips[index],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
