import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tripleproblematiek/models/tip_model.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_image.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_title.dart';
import 'package:tripleproblematiek/widgets/author.dart';
import 'package:tripleproblematiek/widgets/bullet_point_widget/bullet_point_output.dart';
import 'package:tripleproblematiek/widgets/category.dart';
import 'package:tripleproblematiek/widgets/text/paragraph_title.dart';
import 'package:zefyr/zefyr.dart';

class TipScreen extends StatelessWidget {
  final Tip tip;
  final int index;

  TipScreen({this.tip, this.index});

  @override
  Widget build(BuildContext context) {
    print(tip);
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ArticleTitle(
                  text: tip.title,
                  index: index,
                  inListView: false,
                ),
                SizedBox(height: 25),
                Container(
                  height: 225,
                  width: double.infinity,
                  child: ArticleImage(
                    index: index,
                    small: false,
                    encode: tip.cover,
                  ),
                ),
                SizedBox(height: 25),
                ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: Text(
                    tip.description,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                      height: 1.65,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                ParagraphTitle("Do's"),
                SizedBox(height: 5),
                BulletPointOutput(
                  tip.positiveBulletPoints,
                  Icon(
                    Icons.check,
                    size: 16,
                    color: Color(0xFF00A81B),
                  ),
                ),
                SizedBox(height: 15),
                ParagraphTitle("Dont's"),
                SizedBox(height: 5),
                BulletPointOutput(
                  tip.negativeBulletPoints,
                  Icon(
                    Icons.close,
                    size: 16,
                    color: Color(0xFFC71010),
                  ),
                ),
                SizedBox(height: 35),
                Author(tip.owner, tip.date),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
