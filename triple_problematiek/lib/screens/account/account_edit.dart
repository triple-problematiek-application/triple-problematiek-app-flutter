import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tripleproblematiek/models/user.model.dart';
import 'package:tripleproblematiek/screens/account/widgets/account_card.dart';
import 'package:tripleproblematiek/screens/account/widgets/edit_field.dart';
import 'package:tripleproblematiek/services/network_services/authentication_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';
import 'package:tripleproblematiek/widgets/buttons/button.dart';
import 'package:tripleproblematiek/widgets/image_picker.dart';

class AccountEditInfo extends StatefulWidget {

  @override
  _AccountEditInfoState createState() => _AccountEditInfoState();
}

class _AccountEditInfoState extends State<AccountEditInfo> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _firstName = TextEditingController(text: SharedPreferencesService.user.firstName);

  TextEditingController _lastName = TextEditingController(text: SharedPreferencesService.user.lastName);

  TextEditingController _function = TextEditingController(text: SharedPreferencesService.user.function);

  TextEditingController _email = TextEditingController(text: SharedPreferencesService.user.emailAddress);

  TextEditingController _phone = TextEditingController(text: SharedPreferencesService.user.phoneNumber);

  String _image;

  void submit() async {
    User before = SharedPreferencesService.user;
    if (_formKey.currentState.validate()) {
      User user = User(
        firstName: before.firstName != _firstName.text ? _firstName.text : null,
        lastName: before.lastName != _lastName.text ? _lastName.text : null,
        function: before.function != _function.text ? _function.text : null,
        emailAddress: before.emailAddress != _email.text ? _email.text : null,
        phoneNumber: before.phoneNumber != _phone.text ? _phone.text : null,
        profilePicture: before.profilePicture != _image ? _image : null,
      );
      Map<String, dynamic> response = await AuthenticationService.updateUser(user);
      User newUser = User(
        firstName: response['firstName'],
        lastName: response['lastName'],
        function: response['function'],
        emailAddress: response['emailAddress'],
        phoneNumber: response['phoneNumber'],
        isAdministrator: response['administrator'],
        profilePicture: response['profilePicture'],
        password: SharedPreferencesService.user.password,
        token: SharedPreferencesService.user.token,
      );
      SharedPreferencesService.login(newUser);
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      print(response);
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
          child: Form(
            key: _formKey,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Account instellingen',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                  SizedBox(height: 15),
                  ImagePickerWidget((image) => _image = image),
                  SizedBox(height: 25),
                  EditField("Voornaam", _firstName),
                  SizedBox(height: 25),
                  EditField("Achternaam", _lastName),
                  SizedBox(height: 25),
                  EditField("Functie", _function),
                  SizedBox(height: 25),
                  EditField("Emailadres", _email),
                  SizedBox(height: 25),
                  EditField("Telefoonnummer", _phone),
                  SizedBox(height: 35),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        child: SizedBox(
                          width: double.infinity,
                          child: ButtonWidget(
                            title: "Opslaan",
                            onClick: () => submit(),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Flexible(
                        child: SizedBox(
                          width: double.infinity,
                          child: Container(),
                        ),
                      ),
                    ],
                  ),
                ],
            ),
          ),
        ),
      ),
    );
  }
}
