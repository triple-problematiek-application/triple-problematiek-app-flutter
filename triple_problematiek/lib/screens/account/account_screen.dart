import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tripleproblematiek/screens/account/account_edit.dart';
import 'package:tripleproblematiek/screens/account/widgets/account_card.dart';
import 'package:tripleproblematiek/screens/account/widgets/account_option.dart';
import 'package:tripleproblematiek/screens/article/article_list_screen.dart';
import 'package:tripleproblematiek/screens/article/utils/request_type.dart';
import 'package:tripleproblematiek/screens/tip/tip_list_screen.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';
import 'package:zefyr/zefyr.dart';

class AccountScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Mijn account',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                  SizedBox(height: 15),
                  AccountCard(
                      "${SharedPreferencesService.user.firstName} ${SharedPreferencesService.user.lastName}",
                      SharedPreferencesService.user.emailAddress),
                  SizedBox(height: 15),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Color(0xFFCCCCCC), width: 0.0),
                    ),
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(
                          left: 18, right: 18, top: 30, bottom: 30),
                      child: Column(
                        children: <Widget>[
                          SharedPreferencesService.user.isAdministrator ? Column(
                            children: <Widget>[
                              AccountOption("Aanvragen", Icons.inbox, ArticleListScreen(RequestType.UNVERIFIED)),
                              SizedBox(height: 40),
                            ],
                          ) : Container(),
                          AccountOption("Mijn artikelen", Icons.description, ArticleListScreen(RequestType.SELF)),
                          SizedBox(height: 40),
                          AccountOption("Mijn tips", FlutterIcons.md_bulb_ion, TipListScreen(RequestType.SELF)),
                          SizedBox(height: 40),
                          AccountOption("Wijzig profiel", Icons.account_box, AccountEditInfo()),
                          SizedBox(height: 40),
                          AccountOption("Uitloggen", Icons.exit_to_app, null),
                        ],
                      ),
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
