import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

// ignore: must_be_immutable
class AccountOption extends StatelessWidget {

  String _text;
  IconData _icon;
  final Widget location;

  AccountOption(this._text, this._icon, this.location);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: ()  {
        if (location == null) {
          SharedPreferencesService.logout();
          Navigator.of(context).pop();
          Navigator.of(context).pop();
          return;
        }
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => location,
          ),
        );
      },
      child: Row(
        children: <Widget>[
          Icon(_icon,
            color: Color(0xFF000000).withOpacity(0.6),
            size: 20,
          ),
          SizedBox(width: 25),
          Text(_text,
            style: TextStyle(
              height: 1.65,
              fontWeight: FontWeight.w400,
              fontSize: 14.0,
              color: Color(0xFF3E3E3E),
            ),
          ),
          Spacer(),
          Icon(Icons.chevron_right,
            color: Color(0xFF000000).withOpacity(0.6),
          ),
        ],
      ),
    );
  }
}