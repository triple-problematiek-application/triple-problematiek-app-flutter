import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

// ignore: must_be_immutable
class AccountCard extends StatelessWidget {
  String _name;
  String _emailadress;

  AccountCard(this._name, this._emailadress);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(color: Color(0xFFCCCCCC), width: 0.0),
      ),
      child: Container(
        height: 87,
        width: double.infinity,
        padding: EdgeInsets.only(left: 18, right: 18, top: 21, bottom: 21),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(90),
              child: SharedPreferencesService.user.profilePicture == null
                  ? Image.asset("assets/images/persoon.jpg")
                  : Image.memory(
                      Base64Decoder().convert(
                          SharedPreferencesService.user.profilePicture),
                    ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _name,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16.0,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    _emailadress,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14.0,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
