import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tripleproblematiek/widgets/text/form_field_title.dart';

class EditField extends StatefulWidget {

  final String _text;
  final TextEditingController _controller;

  EditField(this._text, this._controller);

  @override
  _EditFieldState createState() => _EditFieldState();
}

class _EditFieldState extends State<EditField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        FormFieldTitle(widget._text),
        SizedBox(height: 5,),
        TextFormField(
          validator: (value) {
            if (value.isEmpty) return "${widget._text} is verplicht";
            return null;
          },
          controller: widget._controller,
          decoration: InputDecoration(
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(
                color: Color(0xFFCCCCCC),
                width: 0.5,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(3.0),
            ),
          ),
        ),
      ],
    );
  }
}

