import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ArticleTitle extends StatelessWidget {

  final String text;
  final int index;
  final bool inListView;

  ArticleTitle({this.text, this.index, this.inListView});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "article_title_$index",
      child: Material (
        color: Colors.transparent,
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: inListView ? 14 : 18,
            color: Color(0xFF3E3E3E),
          ),
        ),
      ),
    );
  }
}