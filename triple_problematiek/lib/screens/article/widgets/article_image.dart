import 'dart:convert';

import 'package:flutter/cupertino.dart';

class ArticleImage extends StatelessWidget {
  final bool small;
  final String encode;
  final int index;

  ArticleImage({this.small, this.encode, this.index});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "article_image_$index",
      child: ClipRRect(
        borderRadius: small ? BorderRadius.circular(3.0) : BorderRadius.zero,
        child: encode == null || encode.isEmpty
            ? FittedBox(
                fit: BoxFit.fill,
                child: Image.asset(
                  "assets/images/stockImage.PNG",
                ),
              )
            : FittedBox(
                fit: BoxFit.fill,
                child: Image.memory(
                  Base64Decoder().convert(encode),
                ),
              ),
      ),
    );
  }
}
