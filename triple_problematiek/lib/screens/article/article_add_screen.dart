import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/services/network_services/article_service.dart';
import 'package:tripleproblematiek/widgets/Buttons/button.dart';
import 'package:tripleproblematiek/widgets/Buttons/button_border.dart';
import 'package:tripleproblematiek/widgets/image_picker.dart';
import 'package:tripleproblematiek/widgets/text/form_field_title.dart';
import 'package:tripleproblematiek/widgets/text/form_title.dart';
import 'package:zefyr/zefyr.dart';

// ignore: must_be_immutable
class AddArticle extends StatefulWidget {
  @override
  _AddArticleState createState() => _AddArticleState();

  Article initialArticle;

  AddArticle({this.initialArticle});
}

class _AddArticleState extends State<AddArticle> {
  final _formKey = GlobalKey<FormState>();

  var categories = [
    'Signaleren',
    'Diagnostiek',
    'Begeleiding',
    'Behandeling',
    'Verwijzing'
  ];
  var currentItemSelected;

  String _image;

  TextEditingController _title;

  ZefyrController _controller;
  FocusNode _focusNode;

  ZefyrField _editor;

  void submit() async {
    if (_formKey.currentState.validate()) {
      Article article = new Article(
        _image,
          widget.initialArticle?.id,
          _title.value.text,
          currentItemSelected.toLowerCase(),
          jsonEncode(_controller.document),
          "Patricia Paay",
          DateTime.now());
      Map<String, dynamic> response = widget.initialArticle != null
          ? await ArticleService.updateArticle(article)
          : await ArticleService.postArticle(article);
      if (widget.initialArticle != null) {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      }
      Navigator.of(context).pop();
    }
  }

  @override
  void initState() {
    super.initState();
    _title = TextEditingController();
    _controller = ZefyrController(widget.initialArticle != null
        ? NotusDocument.fromJson(jsonDecode(widget.initialArticle.body))
        : NotusDocument());

    if (widget.initialArticle != null) {
      currentItemSelected = widget.initialArticle?.category[0].toUpperCase() +
          widget.initialArticle.category.substring(1);
      _title.text = widget.initialArticle.title;
    }
    _focusNode = FocusNode();
    _editor = ZefyrField(
      height: 320,
      controller: _controller,
      focusNode: _focusNode,
      autofocus: false,
      decoration: InputDecoration(
        focusedBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0xFFFED42E),
            width: 2,
          ),
        ),
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0xFFCCCCCC),
            width: 0.5,
          ),
        ),
      ),
      physics: ClampingScrollPhysics(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 50),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FormTitle(
                    "Nieuw artikel",
                    "Maak een nieuw artikel aan",
                  ),
                  SizedBox(height: 35),
                  ImagePickerWidget((image) => _image = image),
                  SizedBox(height: 25),
                  FormFieldTitle("Titel"),
                  SizedBox(
                    height: 5,
                  ),
                  TextFormField(
                    controller: _title,
                    validator: (value) {
                      if (value.isEmpty) return 'Voer een titel in';
                      return null;
                    },
                    decoration: InputDecoration(
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color(0xFFCCCCCC),
                          width: 0.5,
                        ),
                      ),
                      hintText: "Artikel titel",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  FormFieldTitle("Bericht"),
                  SizedBox(height: 5),
                  _editor,
                  SizedBox(height: 25),
                  FormFieldTitle("Categorie"),
                  SizedBox(height: 5),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(
                        color: Color(0xFFCCCCCC),
                      ),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButtonFormField<String>(
                        validator: (value) =>
                            value == null ? 'Voer een catogorie in' : null,
                        hint: Text(
                          'Selecteer een categorie',
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 14,
                            color: Color(0xFF3E3E3E),
                          ),
                        ),
                        items: categories.map(
                          (String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                              value: dropDownStringItem,
                              child: Text(dropDownStringItem),
                            );
                          },
                        ).toList(),
                        onChanged: (String newValueSelected) {
                          setState(
                            () {
                              this.currentItemSelected = newValueSelected;
                            },
                          );
                        },
                        value: currentItemSelected,
                      ),
                    ),
                  ),
                  SizedBox(height: 35),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        child: SizedBox(
                          width: double.infinity,
                          child: ButtonWidget(
                            title: "Opslaan",
                            onClick: () => submit(),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Flexible(
                        child: SizedBox(
                          width: double.infinity,
                          child: ButtonBorderWidget("Annuleren"),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
