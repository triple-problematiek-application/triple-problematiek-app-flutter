import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/screens/article/utils/request_type.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_card.dart';
import 'package:tripleproblematiek/services/network_services/article_service.dart';
import 'package:tripleproblematiek/widgets/searchbar.dart';

import 'article_add_screen.dart';

class ArticleListScreen extends StatefulWidget {
  @override
  _ArticleListScreenState createState() => _ArticleListScreenState();

  final RequestType type;

  ArticleListScreen(this.type);
}

class _ArticleListScreenState extends State<ArticleListScreen> {
  List<Article> articles = List();

  @override
  void initState() {
    super.initState();
    loadArticles(null);
  }

  void loadArticles(String query) async {
    Map<String, dynamic> loadedArticles;
    switch (widget.type) {
      case RequestType.NORMAL:
        loadedArticles = await ArticleService.getArticles(query);
        break;
      case RequestType.SELF:
        loadedArticles = await ArticleService.getOwnArticles(query);
        break;
      case RequestType.UNVERIFIED:
        loadedArticles = await ArticleService.getUnverifiedArticles(query);
        break;
    }
    setState(() {
      articles = List();
      for (var articleObject in loadedArticles['articles']) {
        articles.add(
          Article.fromJson(articleObject),
        );
      }
    });
  }

  String getPageTitle() {
    switch (widget.type) {
      case RequestType.NORMAL:
        return "Artikelen";
      case RequestType.SELF:
        return "Mijn artikelen";
      case RequestType.UNVERIFIED:
        return "Niet geverifieerde artikelen";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFFEC5C27),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => AddArticle(),
            ),
          );
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(
            left: 15,
            right: 15,
            top: 42,
            bottom: 27,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                getPageTitle(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 25),
              SearchBarWidget(
                  "Zoek op artikelen", (value) => loadArticles(value)),
              SizedBox(height: 25),
              Container(
                width: double.infinity, //TODO: Check if needed
                child: ListView.builder(
                  itemCount: articles.length,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return ArticleCard(
                      index: index,
                      article: articles[index],
                      type: widget.type,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
