import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/screens/article/article_add_screen.dart';
import 'package:tripleproblematiek/screens/article/utils/request_type.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_image.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_title.dart';
import 'package:tripleproblematiek/services/network_services/article_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';
import 'package:tripleproblematiek/widgets/author.dart';
import 'package:tripleproblematiek/widgets/category.dart';
import 'package:zefyr/zefyr.dart';

class ArticleScreen extends StatelessWidget {
  final Article article;
  final int index;
  final RequestType type;

  ArticleScreen({this.article, this.index, this.type});

  //todo maxime fix error with enter thing

  void delete(BuildContext context) async {
    Map<String, dynamic> loadedArticles =
        await ArticleService.deleteArticle(article.id);
    if (loadedArticles.isNotEmpty) {
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    }
  }

  Widget approveButton(BuildContext context) {
    if (type == RequestType.UNVERIFIED)
      return FlatButton(
        child: Text("approv dis"),
        onPressed: () async {
          // verify..
          Map<String, dynamic> response =
              await ArticleService.verifyArticle(article);
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        },
      );
    else
      return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: ZefyrScaffold(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ArticleTitle(
                  text: article.title,
                  index: index,
                  inListView: false,
                ),
                SizedBox(height: 25),
                Container(
                  width: double.infinity,
                  height: 225,
                  child: ArticleImage(
                    index: index,
                    small: false,
                    encode: article.cover,
                  ),
                ),
                SizedBox(height: 25),
                ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: ZefyrView(
                    document: NotusDocument.fromJson(jsonDecode(article.body)),
                  ),
                ),
                SizedBox(height: 25),
                Author(article.owner, article.date),
                SizedBox(height: 25),
                CategoryWidget(article.category),
                SizedBox(height: 25),
                (SharedPreferencesService.user.isAdministrator ||
                        SharedPreferencesService.user.firstName +
                                ' ' +
                                SharedPreferencesService.user.lastName ==
                            article.owner)
                    ? Row(
                        children: <Widget>[
                          FlatButton(
                            child: Text("Bewerken"),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      AddArticle(initialArticle: article),
                                ),
                              );
                            },
                          ),
                          FlatButton(
                            child: Text("Verwijderen"),
                            onPressed: () {
                              delete(context);
                              //todo
                            },
                          ),
                        ],
                      )
                    : Container(),
                approveButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
