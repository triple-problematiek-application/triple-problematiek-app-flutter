import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExpandableTextWidget extends StatefulWidget {
  final String title;
  final String description;
  ExpandableController controller = new ExpandableController();

  final Function(ExpandableTextWidget) onStateChange;

  ExpandableTextWidget({this.title, this.description, this.onStateChange});

  @override
  _ExpandableTextWidgetState createState() => _ExpandableTextWidgetState();
}

class _ExpandableTextWidgetState extends State<ExpandableTextWidget> {

  @override
  void initState() {
    super.initState();

    widget.controller.addListener(() {
      print('stage 1');
      widget.onStateChange(widget);
      print('${widget.title} is now ${widget.controller.expanded ? 'open' : 'closed'}');
    });
  }

  @override
  Widget build(BuildContext context) {
    return ExpandablePanel(
      controller: widget.controller,
      theme: ExpandableThemeData(
        bodyAlignment: ExpandablePanelBodyAlignment.center,
        headerAlignment: ExpandablePanelHeaderAlignment.center,
        iconColor: Color(0xFF3e3e3e).withOpacity(0.8),
        alignment: Alignment.center,
      ),
      header: Container(
        width: double.infinity,
        height: 52,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                  color: Color(0xFF3E3E3E),
                ),
              ),
            ],
          ),
        ),
      ),
      expanded: Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 40),
        child: Text(
          widget.description,
          style: TextStyle(
            height: 1.65,
            fontWeight: FontWeight.w400,
            fontSize: 14.0,
            color: Color(0xFF3E3E3E),
          ),
        ),
      ),
    );
  }
}
