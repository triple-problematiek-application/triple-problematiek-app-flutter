import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cube/flutter_cube.dart';
import 'package:tripleproblematiek/screens/cube/widgets/expandable_text_widget.dart';

class CubeScreen extends StatefulWidget {
  @override
  _CubeScreenState createState() => _CubeScreenState();
}

class _CubeScreenState extends State<CubeScreen> with TickerProviderStateMixin {
  Vector3 rotation = new Vector3(0, 0, 0);
  Vector3 scale = new Vector3(1, 1, 1);

  AnimationController _controller;
  AnimationController _singleFocusController;
  var cubeAnimation;
  var focusAnimation;

  Scene _scene;
  Object _baseObject;

  Map<ExpandableTextWidget, bool> textWidgets = Map();

  Map<Object, String> cubes = Map();
  Map<Object, Vector3> cubeGoal = Map();
  Map<Object, Vector3> cubeStart = Map();

  //Vector3 notHere = Vector3(10, 10, 10);

  void createCube(Vector3 basePostion, Vector3 goalPosition, String texture) {
    Object cube = Object(
        fileName: 'assets/cube/cube_$texture.obj',
        scale: scale,
        position: basePostion);
    cubes[cube] = texture;
    cubeGoal[cube] = goalPosition;
    cubeStart[cube] = basePostion;
  }

  @override
  void initState() {
    super.initState();
    createCube(Vector3(0.0, 1.5, -0.5), Vector3(0, 1.0, 0), "c");
    createCube(Vector3(-1.0, 1.5, -0.5), Vector3(-1.5, 1.25, 0), "a");
    createCube(Vector3(0.0, 1.5, -1.5), Vector3(0, 1, -1.5), "d"); //
    createCube(Vector3(-1.0, 1.5, -1.5), Vector3(-1.5, 1, -1.5), "b");

    createCube(Vector3(0.0, 2.5, -0.5), Vector3(0, 2.5, 0), "g");
    createCube(Vector3(-1.0, 2.5, -0.5), Vector3(-1.5, 2.75, 0), "e");
    createCube(Vector3(0.0, 2.5, -1.5), Vector3(0, 2.5, -1.5), "h"); //
    createCube(Vector3(-1.0, 2.5, -1.5), Vector3(-1.5, 2.5, -1.5), "f");
    _baseObject = new Object(
      rotation: Vector3(-5, 45, -5),
      position: Vector3(1, -2, 1),
      children: cubeGoal.keys.toList(),
    );

    textWidgets[ExpandableTextWidget(
        title: "A",
        description: "Beschrijving van kubus A",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "B",
        description: "Beschrijving van kubus B",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "C",
        description: "Beschrijving van kubus C",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "D",
        description: "Beschrijving van kubus D",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "E",
        description: "Beschrijving van kubus E",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "F",
        description: "Beschrijving van kubus F",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "G",
        description: "Beschrijving van kubus G",
        onStateChange: (widget) => handleFocus(widget))] = false;
    textWidgets[ExpandableTextWidget(
        title: "H",
        description: "Beschrijving van kubus H",
        onStateChange: (widget) => handleFocus(widget))] = false;

    handleAnimations();
  }

  ExpandableTextWidget selectedWidget = null;
  ExpandableTextWidget lastSelectedWidget = null;

  void handleFocus(ExpandableTextWidget widget) {
    print('stage 2');
    textWidgets[widget] = !textWidgets[widget];
    if (textWidgets[widget])
      selectedWidget = widget;
    else {
      selectedWidget = null; //TODO: CLOSE REST!!!!!!
      lastSelectedWidget = widget;
    }
    //TODO: Close rest!!!!!!!!
    focusAnimation = new Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(new CurvedAnimation(
        parent: _singleFocusController, curve: Curves.linear));
    _singleFocusController.reset();
    _singleFocusController.forward();
  }

  void handleAnimations() {
    print("initializing animations");
    _singleFocusController =
        AnimationController(duration: Duration(milliseconds: 1000), vsync: this)
          ..addListener(() {
            for (Object object in cubes.keys) {
              Vector3 finalPosition = cubeGoal[object];
              if (selectedWidget == null) {
                //TODO: Assume coming from last selected
                Map selected = Map.from(cubes)
                  ..removeWhere((key, value) =>
                      value != lastSelectedWidget.title.toLowerCase());
                Object selectedCube = selected.keys.first;
                selectedCube.scale.setFrom(Vector3(2,2,2) - (Vector3(1,1,1) * focusAnimation.value));
                selectedCube.rotation.setFrom(Vector3(-360 , -360 , -360) * focusAnimation.value);
                finalPosition = cubeGoal[selectedCube] +
                    ((cubeGoal[object] - cubeGoal[selectedCube]) *
                        focusAnimation.value);
              } else if (cubes[object] != selectedWidget.title.toLowerCase()) {
                Map selected = Map.from(cubes)
                  ..removeWhere((key, value) =>
                      value != selectedWidget.title.toLowerCase());
                Object selectedCube = selected.keys.first;
                finalPosition = cubeGoal[object] +
                    ((cubeGoal[selectedCube] - cubeGoal[object]) *
                        focusAnimation.value);
              } else {
                Map selected = Map.from(cubes)..removeWhere((key, value) =>
                  value != selectedWidget.title.toLowerCase());
                Object selectedCube = selected.keys.first;
                selectedCube.scale.setFrom(Vector3(1,1,1) + (Vector3(1,1,1) * focusAnimation.value));
                selectedCube.rotation.setFrom(Vector3(360 , 360 , 360) * focusAnimation.value);
              }
              object.position.setFrom(finalPosition);
              object.updateTransform();
            }
            _baseObject.updateTransform();
            _scene.update();
          });

    _controller =
        AnimationController(duration: Duration(milliseconds: 2500), vsync: this)
          ..addListener(() {
            for (Object object in cubeGoal.keys.toList()) {
              Vector3 finalPosition = cubeStart[object] +
                  ((cubeGoal[object] - cubeStart[object]) *
                      cubeAnimation.value);
              object.position.setFrom(finalPosition);
              object.updateTransform();
            }
            _baseObject.updateTransform();
            _scene.update();
          })
          ..forward();
    cubeAnimation = new Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(
        new CurvedAnimation(parent: _controller, curve: Curves.easeInOutBack));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFFED42E),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Cube(
              interactive: true,
              onSceneCreated: (Scene scene) {
                _scene = scene;
                _scene.camera.zoom = 2.0;
                scene.world.add(_baseObject);
              },
            ),
          ),
          Expanded(
            flex: 6,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(left: 22, right: 22),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'De kubus van der Nagel',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ullamcorper in non porttitor nulla varius aliquam. Leo ac nec non at suscipit a amet. Nibh tempus convallis eu, sapien, adipiscing faucibus eleifend condimentum quam. Diam purus dictumst commodo facilisi cursus dignissim sed tristique. Massa pulvinar ornare ipsum nulla sit. Enim adipiscing arcu nunc, lectus. Ut tincidunt nulla aliquet vel rhoncus pretium mauris sed. Pellentesque quis cras viverra rutrum. Eu quis porta felis quam sed amet, pellentesque purus nibh. Velit faucibus quis diam et vel, nam nunc diam, dignissim.',
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 14,
                        height: 1.65,
                        color: Color(0xFF3E3E3E),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    for (ExpandableTextWidget widget in textWidgets.keys)
                      widget,
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
