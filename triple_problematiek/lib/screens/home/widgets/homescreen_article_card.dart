import 'package:flutter/material.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_image.dart';

import '../../../models/article_model.dart';
import '../../article/article_screen.dart';

class HomeScreenCard extends StatelessWidget {

  final Article article;
  final int index;

  HomeScreenCard({this.index, this.article});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          width: 157,
            height: 250,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(3),
                  child: Container(
                    height: 80,
                    width: 150,
                    child: ArticleImage(
                      index: index,
                      small: true,
                      encode: article.cover,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(article.title,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Color(0xFF3E3E3E),
                ),
                ),
              ],
            ),
        ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => Container(
              child: ArticleScreen(
                article: article,
                index: index,
              ),
            ),
          ),
        );
      },
    );
  }
}
