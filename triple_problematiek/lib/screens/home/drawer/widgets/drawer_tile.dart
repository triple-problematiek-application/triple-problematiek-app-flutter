import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ListTileCard extends StatelessWidget {
  String listTileTxt;
  IconData iconName;
  Widget location;

  ListTileCard(this.listTileTxt, this.iconName, this.location);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 15),
      child: ListTile(
        title: Row(
          children: <Widget>[
            SizedBox(width: 10),
            Icon(
              iconName,
              color: Color(0xFF000000).withOpacity(0.6),
            ),
            SizedBox(width: 28),
            Text(
              listTileTxt,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Color(0xFF333333).withOpacity(0.6),
              ),
            ),
          ],
        ),
        onTap: () {
          Navigator.of(context).pop();
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => location,
            ),
          );
        },
      ),
    );
  }
}