import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/screens/account/account_screen.dart';
import 'package:tripleproblematiek/screens/authentication/login_screen.dart';
import 'drawer_tile.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class AuthTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SharedPreferencesService.isLoggedIn()
        ? LoggedInTile()
        : ListTileCard('Login', Icons.person, LoginScreen());
  }
}

class LoggedInTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: ListTile(
        title: Row(
          children: <Widget>[
            SizedBox(width: 10),
            Icon(
                Icons.person,
                color: Color(0xFF000000).withOpacity(0.6),
            ),
            SizedBox(width: 26),
            Text(
              'Mijn account',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: Color(0xFF333333).withOpacity(0.6),
              ),
            ),
          ],
        ),
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => AccountScreen()));
        },
      ),
    );
  }
}
