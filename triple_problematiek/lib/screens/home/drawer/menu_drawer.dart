import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tripleproblematiek/screens/article/article_list_screen.dart';
import 'package:tripleproblematiek/screens/article/utils/request_type.dart';
import 'package:tripleproblematiek/screens/contact/contact_screen.dart';
import 'package:tripleproblematiek/screens/cube/cube_screen.dart';
import 'package:tripleproblematiek/screens/disorder/disorder_list_screen.dart';
import 'package:tripleproblematiek/screens/home/drawer/widgets/drawer_tile.dart';
import 'package:tripleproblematiek/screens/questions/forum_screen.dart';
import 'package:tripleproblematiek/screens/tip/tip_list_screen.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

import '../home_screen.dart';
import 'widgets/drawer_auth_tile.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        DrawerHeader(
//          child: SharedPreferencesService.isLoggedIn()
//              ? Container()
//              : (SharedPreferencesService.user.profilePicture == null
//                  ? ClipRRect(
//                      borderRadius: BorderRadius.circular(50),
//                      child: Image.asset("assets/images/persoon.jpg"),
//                    )
//                  : ClipRRect(
//                      borderRadius: BorderRadius.circular(50),
//                      child: Image.memory(
//                        Base64Decoder().convert(
//                          SharedPreferencesService.user.profilePicture,
//                        ),
//                        fit: BoxFit.fill,
//                      ),
//                    )),
          decoration: BoxDecoration(
            color: Color(0xFFF9E7C3),
          ),
        ),
        ListTileCard(
          'Home',
          Icons.home,
          HomeScreen(),
        ),
        ListTileCard(
          'Artikelen',
          Icons.insert_drive_file,
          ArticleListScreen(RequestType.NORMAL),
        ),
        ListTileCard(
          'Stoornissen',
          FlutterIcons.medical_bag_mco,
          DisorderListScreen(),
        ),
        ListTileCard(
          'Tips',
          FlutterIcons.md_bulb_ion,
          TipListScreen(RequestType.NORMAL),
        ),
        ListTileCard(
          'Kubus',
          FlutterIcons.cube_faw,
          CubeScreen(),
        ),
        ListTileCard(
          'Contact',
          Icons.perm_contact_calendar,
          ContactScreen(),
        ),
        AuthTile(),
      ],
    );
  }
}
