import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:tripleproblematiek/models/article_model.dart';
import 'package:tripleproblematiek/screens/home/drawer/menu_drawer.dart';
import 'package:tripleproblematiek/screens/home/widgets/homescreen_article_card.dart';
import 'package:tripleproblematiek/services/network_services/article_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';
import 'package:tripleproblematiek/widgets/category.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  _launchURL() async {
    const url =
        'https://www.bol.com/nl/p/praktijkboek-triple-problematiek/9200000085676497/?Referrer=ADVNLGOO002008N-G-87212219186-S-852420302860-9200000085676497&gclid=Cj0KCQjwlN32BRCCARIsADZ-J4t4J-Fw0RjyQcfcK5kccnHAEwgW4ic5t1JpikV6HTdITcDTbHMnAjgaAl5oEALw_wcB';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: MenuDrawer(),
      ),
      appBar: AppBar(
        title: Text("Home screen"),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(15, 42, 15, 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(7),
                    child: Image(
                      image: AssetImage('assets/images/boek.jpg'),
                      height: 125,
                      width: 89,
                    ),
                  ),
                  SizedBox(width: 19),
                  Container(
                    height: 125,
                    width: 208,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Triple Problematiek',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Color(0xFF3E3E3E),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Psychische klachten, verslaving en licht verstandelijke beperking',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w300,
                            height: 1.65,
                            color: Color(0xFF3E3E3E),
                          ),
                        ),
                        SizedBox(height: 15),
                        GestureDetector(
                          onTap: () {
                            _launchURL();
                          },
                          child: Text(
                            'Bekijk het boek hier',
                            style: TextStyle(
                              fontStyle: FontStyle.italic,
                              fontSize: 12,
                              height: 1.65,
                              color: Color(0xFF3E3E3E),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 35),
              Text(
                "Welkom bij Triple Problematiek",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 15),
              ClipRRect(
                borderRadius: BorderRadius.circular(3),
                child: Image(
                  image: AssetImage('assets/images/home_screen.jpg'),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'De Triple Problematiek applicatie is een applicatie die ontwikkeld is om inzicht te geven over bepaalde stoornissen, verslavingen of aandoening. Dit wordt gedaan aan de hand van artikelen die geschreven zijn door gebruikers van de applicatie. Zo kan er in de applicatie worden gevonden hoe men moet omgaan met mensen die een betaald type stoornis, licht geestelijke handicap, of een vorm van een verslaving hebben. Zo kan er snel worden bepaald hoe men ermee moet omgaan. Maar dat is niet het enige, ook is er in de applicatie te vinden hoe men over een bepaald probleem moet heenkomen, zoals wat te doen bij een verslaving. Zo kan men snel en zelfstandig actie verrichten, zodat zij zich weer snel terug in de samenleving kunnen voegen.',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 35),
              Text(
                'Artikelen',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    height: 1.65,
                    color: Color(0xFF3E3E3E)),
              ),
              Text(
                'Informatie van professionals',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 20),
              SharedPreferencesService.articles.length > 0
                  ? Container(
                      height: 204,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: <Widget>[
                          Row(
                            children: SharedPreferencesService.articles
                                .map((art) => Row(
                                      children: <Widget>[
                                        HomeScreenCard(
                                          article: art,
                                          index: SharedPreferencesService
                                              .articles
                                              .indexOf(art),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                      ],
                                    ))
                                .toList(),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      child: Text(
                        "Er zijn geen artikelen gevonden...",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                          height: 1.65,
                          color: Color(0xFF3E3E3E),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
