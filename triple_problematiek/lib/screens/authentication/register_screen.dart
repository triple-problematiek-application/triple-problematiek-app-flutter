import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/user.model.dart';
import 'package:tripleproblematiek/screens/authentication/widgets/text_field_style.dart';
import 'package:tripleproblematiek/services/network_services/authentication_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

import 'login_screen.dart';

class RegisterScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  bool passwordIncorect = false;
  bool firstNameIncorrect = false;
  bool lastNameIncorrect = false;
  bool emailIncorrect = false;

  TextEditingController _firstName = new TextEditingController();
  TextEditingController _lastName = new TextEditingController();
  TextEditingController _email = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  TextEditingController _function = new TextEditingController();

  void register(BuildContext context) async {
    passwordIncorect = false;
    firstNameIncorrect = false;
    lastNameIncorrect = false;
    emailIncorrect = false;
    if (_formKey.currentState.validate()) {
      User user = User(firstName: _firstName.value.text,
      lastName: _lastName.value.text,
      function: _function.value.text,
      emailAddress: _email.value.text,
      password: _password.value.text,
      isAdministrator: false);

      Map<String, dynamic> response = await AuthenticationService.registerUser(user);
      print(response);
      if (response['error'] != null) {
        print(response['error']);
        if (response['error'].contains("password")) passwordIncorect = true;
        if (response['error'].contains("firstname")) firstNameIncorrect = true;
        if (response['error'].contains("lastname")) lastNameIncorrect = true;
        if (response['error'].contains("emailAddress")) emailIncorrect = true;
        _formKey.currentState.validate();
        return;
      }
      User loggedIn = User(
        firstName: response['firstName'],
        lastName: response['lastName'],
        function: _function.value.text,
        emailAddress: _email.value.text,
        password: _password.value.text,
        token: response['token'],
        isAdministrator: false,
      );
      print(loggedIn.toJson());
      SharedPreferencesService.login(loggedIn);
      Navigator.of(context).pop();


      print("Login OK, registered with $loggedIn");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xFFEB5F52),
      body: Form(
        key: _formKey,
        child: Container(
          margin: EdgeInsets.fromLTRB(74,  MediaQuery.of(context).size.height * 0.1, 74,  MediaQuery.of(context).size.height * 0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              Text(
                'Registreren',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 38),
              Text(
                'Voornaam',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _firstName,
                validator: (value) {
                  if (value.isEmpty) return 'Voer voornaam in';
                  if (firstNameIncorrect) return "Voornaam incorrect";
                  return null;
                },
                decoration: TextFieldStyle("Voornaam"),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 25),
              Text(
                'Achternaam',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _lastName,
                validator: (value) {
                  if (value.isEmpty) return 'Voer achternaam in';
                  if (lastNameIncorrect) return "Achternaam incorrect";
                  return null;
                },
                decoration: TextFieldStyle("Achternaam"),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 25),
              Text(
                'Functie',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _function,
                validator: (value) {
                  if (value.isEmpty) return 'Voer functie in';
                  return null;
                },
                decoration: TextFieldStyle("Functie"),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 25),
              Text(
                'Emailadres',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                  controller: _email,
                  validator: (value) {
                    if (value.isEmpty) return 'Voer email in';
                    if (emailIncorrect) return "Dit emailadres bestaat al";
                    return null;
                  },
                  decoration: TextFieldStyle("naam@email.com"),
                  style: TextStyle(color: Colors.white)),
              SizedBox(height: 25),
              Text(
                'Wachtwoord',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _password,
                obscureText: true,
                decoration: TextFieldStyle("*****"),
                validator: (value) {
                  if (value.isEmpty) return 'Voer wachtwoord in';
                  if (passwordIncorect) return 'Wachtwoord voldoet niet aan de voorwaarden';
                  return null;
                },
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 36),
              ButtonTheme(
                minWidth: double.infinity,
                height: 33,
                buttonColor: Color(0xFFFED42E),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  onPressed: () {
                    register(context);
                  },
                  child: Text('Registreer',
                      style: TextStyle(fontSize: 16, color: Colors.white)),
                ),
              ),
              Spacer(),
              Center(
                child: GestureDetector(
                  child: Text(
                    'Heb je al een account? Log hier in!',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
