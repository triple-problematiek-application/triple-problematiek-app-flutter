import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/user.model.dart';
import 'package:tripleproblematiek/screens/authentication/register_screen.dart';
import 'package:tripleproblematiek/screens/authentication/widgets/text_field_style.dart';
import 'package:tripleproblematiek/services/network_services/authentication_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';

class LoginScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  bool emailIncorrect = false;
  bool passwordIncorrect = false;

  TextEditingController _email = new TextEditingController();
  TextEditingController _password = new TextEditingController();

  void login(BuildContext context) async {
    emailIncorrect = false;
    passwordIncorrect = false;
    if (_formKey.currentState.validate()) {
      User user = User(emailAddress: _email.value.text, password: _password.value.text, isAdministrator: false);

      Map<String, dynamic> response =
          await AuthenticationService.loginUser(user);
      if (response['error'] != null) {
        print(response);
        if (response['error'] == "Cannot read property 'password' of null")
          emailIncorrect = true;
        else
          passwordIncorrect = true;
        _formKey.currentState.validate();
        return;
      }

      User loggedIn = User(
          firstName: response['firstName'],
          emailAddress: _email.value.text,
          function: response['function'],
          isAdministrator: true,
          token: response['token'],
          lastName: response['lastName'],
          password: _password.value.text);

      print(loggedIn.toJson() + "aaaaaaaaaaaaaaaaaa");

      SharedPreferencesService.login(loggedIn);
      print("Login OK, logged in with $loggedIn");
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xFFEB5F52),
      body: Form(
        key: _formKey,
        child: Container(
          margin: EdgeInsets.fromLTRB(
              74,
              MediaQuery.of(context).size.height * 0.1,
              74,
              MediaQuery.of(context).size.height * 0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              Text(
                'Inloggen',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 38,
              ),
              Text(
                'Emailadres',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _email,
                validator: (value) {
                  if (value.isEmpty) return 'Voer uw e-mail in';
                  if (emailIncorrect)
                    return 'Dit emailadres is niet bij ons bekend';
                  return null;
                },
                decoration: TextFieldStyle("naam@email.com"),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 25),
              Text(
                'Wachtwoord',
                style: TextStyle(
                  color: Color(0xFFFED42E),
                  fontSize: 16,
                ),
              ),
              TextFormField(
                controller: _password,
                validator: (value) {
                  if (value.isEmpty) return 'Voer uw wachtwoord in';
                  if (passwordIncorrect) return 'Wachtwoord incorrect';
                  return null;
                },
                obscureText: true,
                decoration: TextFieldStyle("*****"),
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(height: 36),
              ButtonTheme(
                minWidth: double.infinity,
                height: 33,
                buttonColor: Color(0xFFFED42E),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  onPressed: () {
                    login(context);
                  },
                  child: Text('Inloggen',
                      style: TextStyle(fontSize: 16, color: Colors.white)),
                ),
              ),
              Spacer(),
              //SizedBox(height: 38),
              Center(
                child: GestureDetector(
                  child: Text(
                    'Nog geen account? Meld je hier aan!',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterScreen()),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
