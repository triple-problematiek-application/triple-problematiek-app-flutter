import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextFieldStyle extends InputDecoration {

  final String _hintText;

  TextFieldStyle(this._hintText);

  @override
  String get hintText => _hintText;

  @override
  InputBorder get focusedBorder => UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  );

  @override
  InputBorder get errorBorder => UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  );

  @override
  InputBorder get focusedErrorBorder => UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  );

  @override
  TextStyle get errorStyle => TextStyle(color: Colors.white);

  @override
  TextStyle get hintStyle => TextStyle(
    color: Colors.white,
    fontSize: 16,
  );

  @override
  InputBorder get enabledBorder => UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.white)
  );

}