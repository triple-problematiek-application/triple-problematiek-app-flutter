import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/disorder_model.dart';
import 'package:tripleproblematiek/screens/article/widgets/article_title.dart';
import 'package:tripleproblematiek/widgets/list_item_initial_cover.dart';
import '../disorder_screen.dart';

class DisorderCard extends StatelessWidget {
  final Disorder disorder;
  final int index;

  DisorderCard({this.disorder, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: InkWell(
        onTap: () => {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => DisorderScreen(
                disorder: disorder,
                index: index,
              ),
            ),
          ),
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
        InitialCover(disorder.title),
            Expanded(
              flex: 7,
              child: Container(
                padding: EdgeInsets.only(left: 15, right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ArticleTitle(
                      text: disorder.title,
                      index: index,
                      inListView: true,
                    ),
                    SizedBox(height: 3),
                    Text(
                      disorder.definition,
                      maxLines: 2,
                      style: TextStyle(
                        fontWeight: FontWeight.w300,
                        height: 1.65,
                        color: Color(0xFF3E3E3E),
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
