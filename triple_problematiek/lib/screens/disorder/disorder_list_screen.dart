import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/disorder_model.dart';
import 'package:tripleproblematiek/screens/disorder/disorder_add_screen.dart';
import 'package:tripleproblematiek/screens/disorder/widgets/disorder_list_item.dart';
import 'package:tripleproblematiek/services/network_services/disorder_service.dart';
import 'package:tripleproblematiek/services/shared_preferences_service.dart';
import 'package:tripleproblematiek/widgets/searchbar.dart';

class DisorderListScreen extends StatefulWidget {
  @override
  _DisorderListScreenState createState() => _DisorderListScreenState();
}

class _DisorderListScreenState extends State<DisorderListScreen> {
  List<Disorder> disorders = List();

  @override
  void initState() {
    super.initState();
    loadDisorders(null);
  }

  void loadDisorders(String query) async {
    Map<String, dynamic> loadedDisorders =
        await DisorderService.getDisorders(query);
    print(loadedDisorders);
    setState(() {
      disorders = List();
      for (var disorderObject in loadedDisorders['disorders']) {
        disorders.add(Disorder.fromJson(disorderObject));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SharedPreferencesService.user.isAdministrator
          ? FloatingActionButton(
              backgroundColor: Color(0xFFEC5C27),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) => AddDisorder(),
                  ),
                );
              },
              child: Icon(Icons.add, color: Colors.white),
            )
          : Container(),
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 27),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Stoornissen',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 25),
              SearchBarWidget("Zoek op een stoornis", (value) => loadDisorders(value)),
              SizedBox(height: 25),
              Container(
                width: double.infinity, //TODO: Check if needed
                child: ListView.builder(
                  itemCount: disorders.length,
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return DisorderCard(
                      index: index,
                      disorder: disorders[index],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
