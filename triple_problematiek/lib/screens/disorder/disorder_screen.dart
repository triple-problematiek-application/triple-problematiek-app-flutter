import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/disorder_model.dart';
import 'package:tripleproblematiek/widgets/bullet_point_widget/bullet_point_output.dart';

// ignore: must_be_immutable
class DisorderScreen extends StatelessWidget {
  Disorder disorder;
  int index;

  DisorderScreen({this.disorder, this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(disorder.title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      height: 1.65,
                      color: Color(0xFF3E3E3E))),
              SizedBox(height: 5),
              Text(disorder.definition,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14,
                    height: 1.65,
                    fontStyle: FontStyle.italic,
                    color: Color(0xFF7A7A7A),
                  )),
              SizedBox(height: 25),
              Text(disorder.description,
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 14,
                      height: 1.65,
                      color: Color(0xFF3E3E3E))),
              SizedBox(height: 25),
              Text(
                'Symptomen',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
              SizedBox(height: 15),
              BulletPointOutput(
                disorder.symptoms,
                Icon(
                  Icons.chevron_right,
                  size: 16,
                  color: Color(0xFF3e3e3e),
                ),
              ),
              SizedBox(height: 25),
              Text(
                'Omgaan met',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Color(0xFF3E3E3E),
                  height: 1.65,
                ),
              ),
              SizedBox(height: 15),
              Text(
                disorder.dealingWith,
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14,
                    height: 1.65,
                    color: Color(0xFF3E3E3E)),
              ),
              SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }
}
