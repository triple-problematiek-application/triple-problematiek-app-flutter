import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/models/disorder_model.dart';
import 'package:tripleproblematiek/services/network_services/disorder_service.dart';
import 'package:tripleproblematiek/widgets/bullet_point_widget/bullet_point_input.dart';
import 'package:tripleproblematiek/widgets/buttons/button.dart';
import 'package:tripleproblematiek/widgets/buttons/button_border.dart';
import 'package:tripleproblematiek/widgets/text/form_field_title.dart';
import 'package:tripleproblematiek/widgets/text/form_title.dart';
import 'package:zefyr/zefyr.dart';

class AddDisorder extends StatefulWidget {
  @override
  _AddDisorderState createState() => _AddDisorderState();
}

class _AddDisorderState extends State<AddDisorder> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _title = TextEditingController();

  final TextEditingController _definition = TextEditingController();

  final TextEditingController _description = TextEditingController();

  final TextEditingController _dealingWith = TextEditingController();

  final BulletPoint _bulletPoint =
      BulletPoint("Symptomen", "Voeg symptoom toe");

  void submit() async {
    if (_formKey.currentState.validate()) {
      Disorder disorder = new Disorder(
        _title.text,
        _definition.text,
        _description.text,
        _bulletPoint.getPoints(),
        _dealingWith.text,
      );
      Map<String, dynamic> response = await DisorderService.postDisorder(disorder);
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        backgroundColor: Color(0xFFFED42E),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 15, right: 15, top: 42, bottom: 42),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FormTitle(
                  "Nieuwe stoornis informatie",
                  "Voeg een nieuwe stoornis toe",
                ),
                SizedBox(height: 25),
                FormFieldTitle('Stoornis'),
                SizedBox(height: 5),
                TextFormField(
                  maxLines: 1,
                  controller: _title,
                  validator: (value) {
                    if (value.isEmpty) return "Vul een naam in";
                    return null;
                  },
                  decoration: InputDecoration(
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Color(0xFFCCCCCC),
                        width: 0.5,
                      ),
                    ),
                    hintText: "Stoornis naam",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                FormFieldTitle('Definitie'),
                SizedBox(height: 5),
                TextFormField(
                  maxLines: 1,
                  validator: (value) {
                    if (value.isEmpty) return "Vul een definitie in";
                    return null;
                  },
                  controller: _definition,
                  decoration: InputDecoration(
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Color(0xFFCCCCCC),
                        width: 0.5,
                      ),
                    ),
                    hintText: "Stoornis definitie",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                FormFieldTitle('Omschrijving'),
                SizedBox(height: 5),
                TextFormField(
                  controller: _description,
                  validator: (value) {
                    if (value.isEmpty) return "Vul een beschrijving in";
                    return null;
                  },
                  maxLines: 6,
                  decoration: InputDecoration(
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Color(0xFFCCCCCC),
                        width: 0.5,
                      ),
                    ),
                    hintText: "Stoornis omschrijving",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
                SizedBox(height: 25),
                FormFieldTitle('Symptomen'),
                SizedBox(height: 5),
                _bulletPoint,
                SizedBox(height: 25),
                FormFieldTitle('Omgaan met'),
                SizedBox(height: 5),
                TextFormField(
                  controller: _dealingWith,
                  validator: (value) {
                    if (value.isEmpty)
                      return "Vul in hoe je om kunt gaan met deze stoornis";
                    return null;
                  },
                  maxLines: 5,
                  decoration: InputDecoration(
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Color(0xFFCCCCCC),
                        width: 0.5,
                      ),
                    ),
                    hintText: "Omgaan met de stoornis",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
                SizedBox(height: 35),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonWidget(
                            title: "Opslaan", onClick: () => submit()),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Flexible(
                      child: SizedBox(
                        width: double.infinity,
                        child: ButtonBorderWidget("Annuleren"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
