import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class InitialCover extends StatelessWidget {
  String title;

  InitialCover(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 75.0,
      height: 75.0,
      color: Color(0xFFEB5F52),
      child: Center(
        child: Text(
          '${title[0].toUpperCase()}',
          style: TextStyle(
              color: Colors.white,
              fontSize: 32
          ),
        ),
      ),
    );
  }
}