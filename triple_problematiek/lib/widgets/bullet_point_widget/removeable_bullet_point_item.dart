import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RemoveableBulletPointItem extends StatelessWidget {
  final String title;
  final Function(RemoveableBulletPointItem) remove;

  RemoveableBulletPointItem(this.title, this.remove);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(left: 15, right: 0, top: 5, bottom: 5),
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFFCCCCCC),
          width: 0.5,
        ),
        borderRadius: BorderRadius.circular(3),
      ),
      child: Row(
        children: <Widget>[
          Text(title),
          Spacer(),
          IconButton(
            icon: Icon(
              Icons.remove,
              color: Color(0xFF3e3e3e).withOpacity(0.6),
            ),
            onPressed: () {
              remove(this);
            },
          )
        ],
      ),
    );
  }

  String toJson() => title;
}
