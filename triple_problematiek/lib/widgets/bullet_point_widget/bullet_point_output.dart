import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class BulletPointOutput extends StatelessWidget {
  List<String> bulletPoints = new List();
  Widget icon;

  BulletPointOutput(this.bulletPoints, this.icon);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: bulletPoints.length,
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext ctxt, int index) {
        return Container(
          margin: const EdgeInsets.only(bottom: 15.0),
          child: Row(
            children: <Widget>[
              icon,
              SizedBox(width: 10),
              Text(
                bulletPoints[index],
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 14,
                  height: 1.65,
                  color: Color(0xFF3E3E3E),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
