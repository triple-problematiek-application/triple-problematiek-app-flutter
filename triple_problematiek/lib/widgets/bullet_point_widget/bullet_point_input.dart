import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tripleproblematiek/widgets/bullet_point_widget/removeable_bullet_point_item.dart';

// ignore: must_be_immutable
class BulletPoint extends StatefulWidget {
  final String title;
  final String addText;

  List<String> getPoints() {
    return removeAbleList.map((e) => e.title).toList();
  }

  List<RemoveableBulletPointItem> removeAbleList = List();

  BulletPoint(this.title, this.addText);

  @override
  _BulletPointState createState() => _BulletPointState();
}

class _BulletPointState extends State<BulletPoint> {
  TextEditingController _controller = TextEditingController();

  void removePoint(RemoveableBulletPointItem item) {
    setState(() {
      widget.removeAbleList.remove(item);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          TextField(
            controller: _controller,
            decoration: InputDecoration(
              suffixIcon: Icon(Icons.add),
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(
                  color: Color(0xFFCCCCCC),
                  width: 0.5,
                ),
              ),
              hintText: widget.addText,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(3.0),
              ),
            ),
            onSubmitted: (value) {
              setState(
                () {
                  widget.removeAbleList.add(
                    RemoveableBulletPointItem(
                      value,
                      (widget) => removePoint(widget),
                    ),
                  );
                  _controller.text = "";
                },
              );
            },
          ),
          (widget.removeAbleList.length > 0) ? Padding(
            padding: const EdgeInsets.only(top: 5.0, bottom: 5),
            child: Divider(
              color: Color(0xFFCCCCCC),
            ),
          ) : Container(),
          for (RemoveableBulletPointItem item in widget.removeAbleList) item,
        ],
      ),
    );
  }
}
