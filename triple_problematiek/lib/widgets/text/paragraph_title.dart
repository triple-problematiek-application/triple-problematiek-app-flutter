import 'package:flutter/cupertino.dart';

class ParagraphTitle extends StatelessWidget {
  String _title;

  ParagraphTitle(this._title);

  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16,
        height: 1.65,
        color: Color(0xFF3E3E3E),
      ),
    );
  }
}