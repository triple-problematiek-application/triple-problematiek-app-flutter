import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class FormFieldTitle extends StatelessWidget {
  String _title;

  FormFieldTitle(this._title);

  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16,
        height: 1.65,
        color: Color(0xFF3E3E3E),
      ),
    );
  }
}