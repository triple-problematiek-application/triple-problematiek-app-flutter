import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class FormTitle extends StatelessWidget {
  String _title;
  String _subTitle;

  FormTitle(this._title, this._subTitle);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          _title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 18,
            color: Color(0xFF3E3E3E),
          ),
        ),
        SizedBox(height: 5),
        Text(
          _subTitle,
          style: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 14,
            height: 1.65,
            color: Color(0xFF3E3E3E),
          ),
        ),
      ],
    );
  }
}