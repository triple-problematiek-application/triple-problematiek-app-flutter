

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:image_picker/image_picker.dart';

class ImagePickerWidget extends StatefulWidget {

  // Image picker
  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();

  Function(String) image;

  ImagePickerWidget(this.image);

}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  dynamic _image;

  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      String encoded = Base64Encoder.urlSafe().convert(image.readAsBytesSync().toList());
      widget.image(encoded);
    });
  }

  @override
  Widget build(BuildContext context) {
    return
      //Image + text next to the image
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: _getImage,
            child: Container(
              height: 60,
              width: 60,
              color: Colors.black12,
              child: _image == null ? Icon(
                  Icons.add_a_photo,
                color: Colors.white,
              ) : Image.file(_image),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Afbeelding',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                  SizedBox(height: 4,),
                  SizedBox(
                      width: 250,
                      child: Text(
                          'Kies een afbeelding',
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 14,
                              color: Color(0xFF3E3E3E),
                            ),
                      )
                  ),
                ],
              ),
            ),
          ),
        ],
      );
  }
}