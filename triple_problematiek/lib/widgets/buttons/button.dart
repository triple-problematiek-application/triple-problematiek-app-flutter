import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ButtonWidget extends StatelessWidget {
  final String title;
  final VoidCallback onClick;

  ButtonWidget({this.title, this.onClick});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(3.0),
      ),
      color: Color(0xFFFED42E),
      textColor: Colors.white,
      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
      splashColor: Color(0xFFF5C813),
      onPressed: () {
        onClick();
      },
      child: Text(
        title,
        style: TextStyle(fontSize: 14.0),
      ),
    );
  }
}
