import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ButtonBorderWidget extends StatelessWidget {
  String title;

  ButtonBorderWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0),
          side: BorderSide(color: Color(0xFFCCCCCC))
      ),
      textColor: Colors.white,
      padding: EdgeInsets.only(top: 16.0,bottom: 16.0),
      splashColor: Color(0xFFCCCCCC),
      onPressed: () {
        //
      },
      child: Text(
        title,
        style: TextStyle(
            fontSize: 14.0,
            color: Color(0xFF3E3E3E) ),
      ),
    );
  }
}