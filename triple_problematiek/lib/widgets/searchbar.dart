import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// ignore: must_be_immutable
class SearchBarWidget extends StatefulWidget {
  @override
  _SearchBarWidgetState createState() => _SearchBarWidgetState();

  String placeHolderTxt;
  Function(String) callback;

  SearchBarWidget(this.placeHolderTxt, [this.callback]);
}

class _SearchBarWidgetState extends State<SearchBarWidget> {
  final textController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  void handleSubmission(String text) {
    widget.callback(text);
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onFieldSubmitted: (value) => handleSubmission(value),
      decoration: InputDecoration(
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(
            color: Color(0xFFCCCCCC),
            width: 0.5,
          ),
        ),
        labelText: widget.placeHolderTxt,
        labelStyle: TextStyle(
          color: Color(0xFFCCCCCC),
        ),
        prefixIcon: Padding(
          padding: EdgeInsets.only(top: 0),
          child: Icon(
            Icons.search,
            color: Color(0xFFCCCCCC),
            size: 20,
          ),
        ),
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(3.0),
          borderSide: BorderSide(),
        ),
      ),
    );
  }
}
