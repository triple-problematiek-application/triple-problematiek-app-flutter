import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class Author extends StatelessWidget {
  String _owner;
  var _date;

  Author(this._owner, this._date);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          _owner,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
            height: 1.65,
            color: Color(0xFF3E3E3E),
          ),
        ),
        Text(
          DateFormat("dd-MM-yyyy - kk:mm").format(_date),
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            height: 1.65,
            color: Color(0xFF3E3E3E).withOpacity(0.4),
          ),
        ),
      ],
    );
  }
}