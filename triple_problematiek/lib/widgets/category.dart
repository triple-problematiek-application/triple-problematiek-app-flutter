import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CategoryWidget extends StatelessWidget {
  String category;

  CategoryWidget(this.category);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(30.0),
      child: Container(
        padding: EdgeInsets.only(left: 25, right: 25, top: 6, bottom: 6),
        color: Color(0xFFFED42E),
        child: FittedBox(
          child: Center(
            child: Text(
              category.toUpperCase(),
              style: TextStyle(
                letterSpacing: 1,
                fontWeight: FontWeight.bold,
                fontSize: 10,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
    throw UnimplementedError();
  }
}
